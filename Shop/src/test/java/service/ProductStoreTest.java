package service;

import org.assertj.core.api.Assertions;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.javamoney.moneta.Money.of;

public class ProductStoreTest {
    private Product product;
    static List<Product> listOfProductInStore = new ArrayList<>();
    private Product product1;


    @Test
    void addProduct(){
        String kindOfProduct = "xd";
        int price =20;
        String code = "USD";
        String color = "Red";
        double weight = 2;
        int howMany = 1;



        product = new Product(of(price, code), kindOfProduct, color, weight);
        listOfProductInStore.add(product);
        Assertions.assertThat(listOfProductInStore)
                .isNotEmpty();
    }


}
