package service;

import org.javamoney.moneta.Money;
import org.javamoney.moneta.internal.MoneyAmountBuilder;


public class Product {
    private Money price;
    private String kindOfProduct;
    private String color;
    private double weight;
    private MoneyAmountBuilder amout = new MoneyAmountBuilder();

    public Product(Money price, String kindOfProduct, String color, double weight) {
        this.price = price;
        this.kindOfProduct = kindOfProduct;
        this.color = color;
        this.weight = weight;
    }


//    public Product(Money price, String kindOfProduct, String color) {
//        this.price = price;
//        this.kindOfProduct = kindOfProduct;
//        this.color = color;
//    }
//
//    public Product(Money price, String kindOfProduct) {
//        this.price = price;
//        this.kindOfProduct = kindOfProduct;
//    }
//
//    public Product(String kindOfProduct) {
//        this.kindOfProduct = kindOfProduct;
//        price = Money.of(0, "PLN");
//    }


    public void setPrice(Money price) {
        this.price = price;
    }

    public void setKindOfProduct(String kindOfProduct) {
        this.kindOfProduct = kindOfProduct;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Money getPrice() {
        return price;
    }

    public String getKindOfProduct() {
        return kindOfProduct;
    }

    public String getColor() {
        return color;
    }

    public double getWeight() {
        return weight;
    }

}
