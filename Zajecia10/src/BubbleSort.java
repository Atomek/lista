public class BubbleSort {

    public static void main(String[] args) {


        int[] arrayToBubbleSort = new int[5];
        arrayToBubbleSort[0] = 6;
        arrayToBubbleSort[1] = 5;
        arrayToBubbleSort[2] = 78;
        arrayToBubbleSort[3] = 4;
        arrayToBubbleSort[4] = 5;

        int[] arrayToBubbleSort1 = new int[arrayToBubbleSort.length];

        arrayToBubbleSort1 = bubblesort(arrayToBubbleSort);

        for (int w : arrayToBubbleSort1) {
            System.out.println(w);
        }

    }


    public static int[] bubblesort(int[] array) {

        int size = array.length;
        for (int i = 0; i < size - 1; i++) {
            int counter = 0;
            for (int j = 0; j < size - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    counter++;
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }

            }
            if (counter == 0) {
                break;
            }
        }
        return array;
    }
}
