import java.util.Scanner;

public class BinaryS {
    public static void main(String[] args) {
        int[] arrayToBubbleSort = {23, 43, 34, 23, 532, 234, 12, 13,343,44};
        int[] arrayToBubbleSort1;
        arrayToBubbleSort1=bubblesort(arrayToBubbleSort);

        for (int arrayToPrint :
                arrayToBubbleSort1) {
            System.out.print(arrayToPrint + "\t");
        }

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj cyfrę");
        int digtial = scanner.nextInt();
        findIndexOfNumber(arrayToBubbleSort1, digtial);
    }

    public static int[] bubblesort(int[] array) {
        int size = array.length;
        for (int i = 0; i < size - 1; i++) {
            int counter = 0;
            for (int j = 0; j < size - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    counter++;
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
            if (counter == 0) {
                break;
            }
        }
        return array;
    }

    public static void findIndexOfNumber(int[] arrayOfNumber, int number){
        int l = 0;
        int n = arrayOfNumber.length;
        int p = n - 1;
        while (l <= p) {
            int s = (l + p) / 2;

            if (arrayOfNumber[s] == number) {
                            //podaj wynik
                System.out.println("Odnaleziono element "+number+" pod indeksem "+s);
                        //zakoncz program
                return;
            }

            if (arrayOfNumber[s] < number)
                l = s + 1;
            else
                p = s - 1;
        }

//podaj wynik
        System.out.println("Nie odnaleziono w tablicy elementu " + number);

    }

}
