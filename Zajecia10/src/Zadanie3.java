public class Zadanie3 {
    public static void main(String[] args) {

        System.out.println(nwd(18,24));

    }

    public static int nwd(int x, int y){
        if(x%y==0){
            return y;
        }
        else if (y%x==0){
            return x;
        }
        else {
            int macMax;
            if(x>y){
                macMax = y;
            }
            else{
                macMax = x;
            }
            for(int i = macMax; macMax>1; macMax--){
                if( y%i==0 && x%i==0){
                    return macMax;
                }


            }
        }
        return -1;
    }
}
