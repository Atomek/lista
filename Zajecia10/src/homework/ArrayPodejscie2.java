package homework;

import java.util.Scanner;

public class ArrayPodejscie2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę wierszy i kolum");
        int rows = scanner.nextInt();
        int col = scanner.nextInt();


        final int turnRight=1;
        final int turnDown=2;
        final int turnLeft=3;
        final int up=4;
        int direction=turnRight;

        int[][] array=new int[rows][col];

        for(int i=0;i<rows;i++)
            for(int j=0;j<col;j++)
                array[i][j]=0;
        int w=0;
        int k=0;
        for(int i=1;i<=rows*col;i++)
        {
            array[w][k]=i;
            switch(direction)
            {
                case turnRight:
                    if(k<col-1 && array[w][k+1]==0)
                        k++;
                    else
                    {
                        w++;
                        direction=turnDown;
                    }
                    break;
                case turnDown:
                    if(w<rows-1 && array[w+1][k]==0)
                        w++;
                    else
                    {
                        k--;
                        direction=turnLeft;
                    }
                    break;
                case turnLeft:
                    if(k>0 && array[w][k-1]==0)
                        k--;
                    else
                    {
                        w--;
                        direction=up;
                    }
                    break;
                case up:
                    if(w>0 && array[w-1][k]==0)
                        w--;
                    else
                    {
                        k++;
                        direction=turnRight;
                    }
                    break;
            }
        }
        for(int i = 0; i<rows; i++){
            for(int j= 0; j<col; j++){
                System.out.print(array[i][j]+ "\t");
            }
            System.out.println();
        }
    }

}
