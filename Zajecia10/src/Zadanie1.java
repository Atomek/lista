import java.util.Scanner;

public class Zadanie1 {

    public static void main(String[] args) {

        getLastDigit(takeIT(),takeIT());

    }

    public static int getLastDigit(int a, int b){
        int result=1;
        for(int i = 0; i<b; i++){
            result=result*a;
        }

        System.out.println(result + "\t\t\t\t" + result%10 );

        return result;
    }

    public static int takeIT(){
        int x;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę: ");
        x=scanner.nextInt();
        return x;
    }

}
