import java.util.Scanner;

public class SubSub {
    public static void main(String[] args) {
        System.out.println("podaj wyraz:");
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
        System.out.println("Podaj podciąg");
        String sign = scanner.nextLine();

        System.out.println(isSubstring(word,sign));

    }

    public static boolean isSubstring(String word, String sign) {
        if(sign.length()>word.length()){
            return false;
        }
        else{
            int x=0;
            for (int counter = 0; counter < word.length(); counter++) {
                int i=counter;
                if(word.charAt(counter)==sign.charAt(0)){
                    while(x<sign.length()){
                        if(word.charAt(counter)==sign.charAt(x)){
                            counter++;
                            x++;
                            if(x==sign.length()-1){
                                return true;
                            }
                        }
                        else{
                            counter=i;
                            x=1;
                        }
                    }
                }
                if(counter==word.length()-1){
                    return false;
                }
            }

        }

        return false;
    }
}



