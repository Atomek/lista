import java.util.Scanner;

public class SubSubSub {
    public static void main(String[] args) {
        System.out.println("podaj wyraz:");
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
        System.out.println("Podaj podciąg");
        String sign = scanner.nextLine();

        System.out.println(isSubstring(word, sign));

    }

    public static boolean isSubstring(String string, String substring) {
        if (substring.length() > string.length()) {
            return false;
        }

        int indexSubstring = 0;
        for (int stringIndex = 0; stringIndex < string.length(); stringIndex++) {
            if (string.charAt(stringIndex) == substring.charAt(0)) {
                while (indexSubstring < substring.length()) {
                    if (substring.charAt(indexSubstring) == string.charAt(stringIndex)) {
                        indexSubstring++;

                        if (indexSubstring == substring.length() - 1) {
                            return true;
                        } else if (stringIndex == string.length()) {
                            return false;
                        }

                    }
                    stringIndex++;

                }


            }

        }

        return false;
    }

}
