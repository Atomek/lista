import javax.swing.*;

public class ClassesAndObjects {
    public static void main(String[] args){
        JLabel label = new JLabel();

        Person john = new Person("John"); //konstruktor
        john.setAge(20);
        //john.name="John";
        Person bob = new Person("Bob");
        bob.setAge(21);
        //bob.name="Bob";

        john.sayHelloTo(bob);
        bob.sayHelloTo(bob);

        System.out.println(john.getName() + " is " + john.getAge() + " years old");
        System.out.println(bob.getName() + " is " + bob.getAge() + " years old");
    }
}
