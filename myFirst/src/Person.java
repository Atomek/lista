public class Person {
    String name;
    static int age;

    public Person(String name){
        this.name = name;
        System.out.println("person created");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getAge() {
        return age;
    }

    public static void setAge(int age) {
        Person.age = age;
    }

    public void sayHelloTo(Person person) {
        System.out.println(getName() + " said Hello to " + person.getName());
    }
}
