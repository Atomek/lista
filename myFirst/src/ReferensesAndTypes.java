public class ReferensesAndTypes {
    public static void main(String[] args) {
        Person john;
        john = new Person("John");
        john.setAge(20);
        celebrateBirthday(john);
        System.out.println(john.getName());

    }

    private static void celebrateBirthday(Person john) {
        john.setAge(john.getAge()+1);
    }

}
