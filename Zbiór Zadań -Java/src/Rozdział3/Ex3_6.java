package Rozdział3;

public class Ex3_6 {
    public static void main(String[] args) {
        System.out.println("Klasa: java.lang.Character");
        System.out.println("Metoda statyczna: digit\n");
        System.out.println("static int digit(int ch, int radix");
        System.out.println("Returns the numeric value of the character ch in the specificed radix.");
        System.out.println();

        char znak[] = {'E', 'u', 'r', 'o', ' ', '2','0','1','2'};

        System.out.println("Watość znaku jako cyfry w układzie dziesiątkowym (radix=10)");
        for (char z :
                znak) {
            System.out.println("Znak: "+z+" Cyfra: "+Character.digit(z,10));
        }
        System.out.println("Uwaga: -1 oznacza, że znak nie jest cyfrą w tym układzie liczbowym");
        System.out.println();
        System.out.println("wartość znaku jako cyfry w układzie szesnastkowym (radix=16)");
        for (char z :
                znak) {
            System.out.println("Znak "+z+" Cyfra: "+Character.digit(z,16));
                   }
            System.out.println("Uwaga: -1 oznacza, że znak nie jest cyfrą w tym układzie liczbowym");

    }
}
