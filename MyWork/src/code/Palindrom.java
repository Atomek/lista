package code;

public class Palindrom {
    public static void main(String[] args) {
        System.out.println(checkPalindrome("Aa"));
        System.out.println(checkPalindrome("Kobyła ma mały bok"));
    }

    static boolean checkPalindrome(String inputString) {

        String withoutSpaces = inputString.replace(" ", "");
        withoutSpaces = withoutSpaces.toLowerCase();
        if (withoutSpaces.length() < 1) {
            return true;
        }
        for (int index = 0; index < withoutSpaces.length() / 2; index++) {
            if (withoutSpaces.charAt(index) != withoutSpaces.charAt(withoutSpaces.length() - 1 - index)) {
                return false;
            }
        }
        return true;
    }
}
