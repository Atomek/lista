package code;

public class Office {
    public static void main(String[] args) {
        int[][] array = new int[4][4];
        array[0][0] = 2;
        array[1][0] = 0;
        array[2][0] = 2;
        array[3][0] = 2;
        array[0][1] = 2;
        array[1][1] = 2;
        array[2][1] = 2;
        array[3][1] = 2;
        array[0][2] = 2;
        array[1][2] = 0;
        array[2][2] = 2;
        array[3][2] = 2;
        array[0][3] = 2;
        array[1][3] = 0;
        array[2][3] = 2;
        array[3][3] = 0;


        System.out.println(matrixElementsSum(array));

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }

    }

    int matrixElementssSum(int[][] matrix) {
        int result = 0;
        for (int indexOfColumn = 0; indexOfColumn < matrix.length; indexOfColumn++) {
            for (int indexOfRows = 0; indexOfRows < matrix.length; indexOfRows++) {

                if(matrix[indexOfRows][indexOfColumn]!=0){
                    result= result+matrix[indexOfRows][indexOfColumn];
                }
                else if(indexOfRows!=matrix.length) {
                    indexOfRows++;
                }
            }
        }
        return result;
    }

    static int matrixElementsSum(int[][] matrix) {
        int s = 0;
        for (int c = 0; c < matrix[0].length; ++c)
            for (int r = 0; r < matrix.length; ++r) {
                if (matrix[r][c] > 0)
                    s+=matrix[r][c];
                else break;
            }
        return s;
    }
}
