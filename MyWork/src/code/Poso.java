package code;

public class Poso {
    public static void main(String[] args) {
        System.out.println(makeArrayConsecutive2(new int[]{2, 3,8, 5}));
    }

    static int makeArrayConsecutive2(int[] statues) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        int result;
        for (int index = 0; index < statues.length ; index++) {
            if(statues[index]>max){
                max = statues[index];
            }
            if(statues[index]<min){
                min=statues[index];
            }
        }
        return max-min-statues.length+1;
    }
}
