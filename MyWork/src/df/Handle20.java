package df;

import behavioral.obserwer.Hendler.Handler;

public class Handle20 implements Handler {
    Handler handler;

    public Handle20(Handler handler) {
        this.handler = handler;
    }

    @Override
    public int handle(int amountOfMoney) {
        System.out.println("20: "+amountOfMoney);
        return amountOfMoney%20;
    }

    @Override
    public Handler nextHandler() {
        return handler;
    }
}