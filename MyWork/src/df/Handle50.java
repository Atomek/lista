package df;

import behavioral.obserwer.Hendler.Handler;

public class Handle50 implements Handler {
    Handler handler;

    public Handle50(Handler handler) {
        this.handler = handler;
    }

    @Override
    public int handle(int amountOfMoney) {
        System.out.println("50: "+amountOfMoney);
        return amountOfMoney%50;
    }

    @Override
    public Handler nextHandler() {
        return handler;
    }
}
