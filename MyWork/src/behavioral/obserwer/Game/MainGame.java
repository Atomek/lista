package behavioral.obserwer.Game;

public class MainGame {
    public static void main(String[] args) {
        Game game = new FootballGame();
        game.startGame();
        Game game1 = new VolleyballGame();
        game1.startGame();
    }
}
