package behavioral.obserwer;

public class ConsoleRandomIntObserver implements Observer{

    @Override
    public void handleSubjectMassage(int randomInteger) {
        System.out.println(randomInteger);
    }
}
