package behavioral.obserwer;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public class FileRandomIntObserver implements Observer {

    PrintWriter printWriter;

    @Override
    public void handleSubjectMassage(int randomInteger) {
        createFile();
        printWriter.print(randomInteger);
        printWriter.close();
    }
     private void createFile(){
         try {
             printWriter = new PrintWriter("teksete.txt");
         } catch (FileNotFoundException e) {
             e.printStackTrace();
         }
     }
}
