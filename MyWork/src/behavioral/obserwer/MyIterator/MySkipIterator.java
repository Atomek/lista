package behavioral.obserwer.MyIterator;

import java.util.Collection;
import java.util.Iterator;

public class MySkipIterator<T> implements MyIterator{

    private Collection<T> collection;
    private Iterator<T> collectionIterator;

    public MySkipIterator(Collection<T> collection) {
        this.collection = collection;
        collectionIterator=collection.iterator();
    }

    @Override
    public boolean hasNext() {
        if (collectionIterator.hasNext()){
            collectionIterator.next();
        }
        return collectionIterator.hasNext();
    }

    @Override
    public Object next() {
        collectionIterator.next();
        return collectionIterator.next();
    }
}
