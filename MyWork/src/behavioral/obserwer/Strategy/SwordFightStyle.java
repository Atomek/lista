package behavioral.obserwer.Strategy;

public class SwordFightStyle implements FightStyle {
    @Override
    public void fight() {
        System.out.println("Fight Fight with Sword");
    }
}
