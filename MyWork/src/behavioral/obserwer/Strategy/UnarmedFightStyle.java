package behavioral.obserwer.Strategy;

public class UnarmedFightStyle implements FightStyle   {
    @Override
    public void fight() {
        System.out.println("Im not fighting");
    }
}
