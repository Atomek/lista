package behavioral.obserwer.Strategy;

import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        Hero hero = new Hero();
        Scanner scanner = new Scanner(System.in);

        hero.fight();

        System.out.println("1. Sword\n2.Bow");

        int fightSwich = scanner.nextInt();

        switch (fightSwich){
            case 1:
                hero.setFightStyle(new SwordFightStyle());
                break;
            case 2:
                hero.setFightStyle(new BowFightStyle());
                break;
        }
        hero.fight();
    }
}
