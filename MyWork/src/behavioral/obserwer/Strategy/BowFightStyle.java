package behavioral.obserwer.Strategy;

public class BowFightStyle implements FightStyle {
    @Override
    public void fight() {
        System.out.println("Fight Fight with Bow");
    }
}
