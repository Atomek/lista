package behavioral.obserwer.Hendler;

public class HandlerMain {
    public static void main(String[] args) {
        Handler handler10 = new GeneralHadler(10,null);
        Handler handler20 = new GeneralHadler(20,handler10);
        Handler handler50 = new GeneralHadler(50, handler20);
        Handler actualProcessingHandler= handler50;
        int money = 880;
        int actualAmount = money;
        while (actualProcessingHandler!=null){
            actualAmount= actualProcessingHandler.handle(actualAmount);
            actualProcessingHandler = actualProcessingHandler.nextHandler();

        }
    }
}
