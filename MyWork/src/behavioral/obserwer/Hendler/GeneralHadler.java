package behavioral.obserwer.Hendler;

public class GeneralHadler implements  Handler {

    int nominal;
    Handler nextHandler;
    public GeneralHadler(int nominal, Handler nextHandler) {
        this.nominal = nominal;
        this.nextHandler = nextHandler;
    }
    @Override
    public int handle(int amountOfMoney) {
        System.out.println("Liczba nominałów " + nominal + ":" + amountOfMoney / nominal);
        return amountOfMoney % nominal;
    }
    @Override
    public Handler nextHandler() {
        return nextHandler;
    }
}