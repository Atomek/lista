package behavioral.obserwer;

public interface Observer {
    void handleSubjectMassage(int randomInteger);
}
