package pz1;

import java.util.Scanner;

public class Zadanie2 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] intArray = new int[10];
        int number = -1;
        int copy;
        while(number!=123){
            copy = number;
            number = scanner.nextInt();
            if(number!=123){
                if(copy>number) number = copy;
                else copy = number;
            }
            else {
                break;
            }

        }

        System.out.println(number);
    }
}
