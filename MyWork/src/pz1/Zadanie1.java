package pz1;

import java.util.Scanner;

public class Zadanie1 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("How many lines?");
        int lines = scanner.nextInt();

        for (int counterLines = 0; counterLines <= lines; counterLines++) {
            for (int counterO = 0; counterO < counterLines; counterO++) {
                System.out.print("o ");
            }
            System.out.println();
        }
    }
}
