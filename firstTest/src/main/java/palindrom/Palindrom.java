package palindrom;

import com.gitlab.Atomek.PalindromMustBeExeption;

public class Palindrom {


    public boolean isPalindrom(String words) {


        if (words == null) {
            throw new PalindromMustBeExeption("Error");
        }
        else if (words.length()<1){
            return true;
        }
        String preperedWords = words.replace(" ", "");
        preperedWords = preperedWords.toLowerCase();

        int size = (preperedWords.length() - 1) / 2;

        for (int indexOfChar = 0; indexOfChar < size; indexOfChar++) {
            if (preperedWords.charAt(indexOfChar) != preperedWords.charAt(preperedWords.length() - 1 - indexOfChar)) {
                return false;
            }
        }
        return true;
    }
}
