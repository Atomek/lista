package com.sda.tests.weater;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

public class WeatherStatisticsService {
    public static WeatherService weatherService;
    public WeatherStatisticsService(WeatherService weatherService){
        this.weatherService=weatherService;
    }

    public int getMeasurmentsCount(){
        return weatherService.getTemperatureList("Kłokock").size();

    }

    public double getAvarangeTemperature(String city){
        List<Double> temperatreList = weatherService.getTemperatureList(city);

        double sum = temperatreList
                .stream()
                .mapToDouble(d->d).sum();
        return sum/temperatreList.size();

    }

    public double getHighesTemperature(String city){
        List<Double> tempertureList= weatherService.getTemperatureList(city);
        OptionalDouble temp = tempertureList
                .stream()
                .mapToDouble(d->d).max();
        return temp.getAsDouble();
    }


    public double getRain(List<String> cityList){
        double rain = cityList
                .stream()
                .mapToDouble(city->weatherService.rainForecastSum(city)).sum();

        return rain;
    }
}
