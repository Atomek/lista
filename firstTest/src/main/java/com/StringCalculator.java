package com;

public class StringCalculator {

    public static int add(String numbers){
        if(numbers.length()==0){
            return 0;
        }
        if(numbers.length()==1){
            return Integer.parseInt(numbers);
        }
        String[] splitStringArrayOfNumbers = numbers.split(" ");
        if(splitStringArrayOfNumbers.length!=0){
            int result = 0;
            for (int index = 0; index < splitStringArrayOfNumbers.length; index++) {
                result= result + Integer.parseInt(splitStringArrayOfNumbers[index]);
            }
            return result;
        }

    }
}
