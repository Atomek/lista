package com.median;

import java.util.Arrays;

public class MedianCalculator {
    public static int getMedian(int[] array){

        if (array==null ||array.length==0 ){
            return -1;
        }
        Arrays.sort(array);
        if (array.length%2==0){
            return ((array[(array.length/2)-1])+array[(array.length/2)])/2;
        }
        if (array.length%2!=0){
            return array[array.length/2];
        }
        return -1;
    }
}
