package com.gitlab.Atomek;

public class Calculator {

    boolean isEven(int number) {
        return number % 2 == 0;
    }

    int divide(int a, int b) {
        return a / b;
    }
}
