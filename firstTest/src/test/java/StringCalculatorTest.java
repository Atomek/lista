import com.StringCalculator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;

public class StringCalculatorTest {


    @ParameterizedTest
    @MethodSource("testDataGenerator")
    void addTest(String numbersInString, int exeptionResult){
        int actual = StringCalculator.add(numbersInString);

        Assertions.assertThat(actual).isEqualTo(exeptionResult);


    }
    static List<Arguments> testDataGenerator() {
        Arguments argument1 = Arguments.of("", 0);
        Arguments argument3 = Arguments.of("1", 1);
        Arguments argument2 = Arguments.of("1 2", 3);
        Arguments argument4 = Arguments.of("12 1", 13);
        Arguments argument5 = Arguments.of("12 1 1 1", 15);

        return Arrays.asList(argument1, argument2, argument3, argument4,argument5);
    }

}
