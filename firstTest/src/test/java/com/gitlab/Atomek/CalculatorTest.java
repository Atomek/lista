package com.gitlab.Atomek;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    public void setUp() {
        calculator = new Calculator();
    }

    @Test
    void isEven() {
        //given
        int testData = 2;
        //when
        boolean actual = calculator.isEven(testData);
        //then
        assertTrue(actual);
    }

    @ParameterizedTest
    @ValueSource(ints = {2, 4, 10, 8})
    void isEvenParametrized(int testData) {
        boolean actual = calculator.isEven(testData);
        assertTrue(actual);
    }


    @ParameterizedTest
    @MethodSource("evenNumberGenerator")
    void isEvenParamtrizedMethodSource(int testData) {
        boolean actual = calculator.isEven(testData);
        assertTrue(actual);
    }

    static List<Integer> evenNumberGenerator() {
        List<Integer> list = new ArrayList<Integer>();
        Random generator = new Random();
        for (int indexNumber = 0; indexNumber < 10; indexNumber++) {
            list.add(generator.nextInt(300) * 2);
        }
        return list;
    }

    @DisplayName(":)")
    @ParameterizedTest
    @MethodSource("testDataGenerator")
    void divide(int a, int b, int expectedresult) {
        int actual = calculator.divide(a, b);
        assertEquals(expectedresult, actual);
    }


    static List<Arguments> testDataGenerator() {
        Arguments argument1 = Arguments.of(3, 3, 1);
        Arguments argument2 = Arguments.of(2, 2, 1);
        Arguments argument3 = Arguments.of(9, 3, 3);
        return Arrays.asList(argument1, argument2, argument3);
    }

    @Disabled
    @ParameterizedTest
    @CsvFileSource(resources = "//testdata.csv", numLinesToSkip = 1)
    void testWithCsvFileSource(int a, int b, int exaResult) {
        int actualResult = calculator.divide(a, b);
        assertEquals(exaResult, actualResult);
    }

}
