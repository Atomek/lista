package com.gitlab.Atomek;

import com.sda.tests.weater.WeatherService;
import com.sda.tests.weater.WeatherStatisticsService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class Weather {

    private WeatherStatisticsService weatherStatisticsService;
    private WeatherService weatherService;

    @BeforeEach
    public void setUp(){
        weatherService = mock(WeatherService.class);
        weatherStatisticsService = new WeatherStatisticsService(weatherService);
    }
    @Test
    public void shouldFindAvangargeTempertaure(){
        List<Double> mockedTemperatures = Arrays.asList(1d,2d,3d);
        when(weatherService.getTemperatureList(anyString()))
                .thenReturn(mockedTemperatures);

        double actulaAvarage = weatherStatisticsService
                .getAvarangeTemperature("Kłokock");

        Assertions.assertThat(actulaAvarage)
                .isEqualTo(2d);
    }
    @Test
    public void shouldFindMaxTemperture(){
        List<Double> mockedTemperatures = Arrays.asList(1d,2d,3d);
        when(weatherService.getTemperatureList(anyString()))
                .thenReturn(mockedTemperatures);

        double maxTempertaure = weatherStatisticsService
                .getHighesTemperature("Kłokock");

        Assertions.assertThat(maxTempertaure)
                .isEqualTo(3d);
    }

    @Test
    public void sumRain(){
        List<String> cityList = Arrays.asList("Płock", "Wrocłąw", "Warszawa");

        when(weatherService.rainForecastSum(anyString())).thenReturn(5d);


        double actual = weatherStatisticsService
                .getRain(cityList);

        Assertions.assertThat(actual)
                .isEqualTo(15d);
    }

}
