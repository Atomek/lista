import java.sql.*;

public class Main {
    //dane do połączenia i wybór sterownika
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL =
            "jdbc:mysql://192.168.64.2:3306/EMP";

    //login i hasło DO BAZY
    private static final String USER = "root";
    private static final String PASS = "aabbccdd";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            //1. Rejestrowanie sterownika
            //Class.forName(JDBC_DRIVER); //-dla starych projektów potrzebne

            //2. Nawiązujemy połączenie
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //3. Tworzymy zapytanie
            stmt = conn.createStatement();
            String sql = "SELECT * FROM Employees";
            //3a. Wykonanie zapytania
            ResultSet rs = stmt.executeQuery(sql);

            //4. Rozpakowanie danych
            while (rs.next()){
                String id = rs.getString("id");
                String firstName = rs.getString("firstname");
                String lastName = rs.getString("lastName");
                int age = rs.getInt("age");

                System.out.printf("Id: %s, Imię: %s, Nazwisko: %s, Wiek: %d\n",
                        id, firstName, lastName, age);
            }
            //5. Sprzątanie - zamknięcie RS
            rs.close();

            //6. Sprzątanie - zamykanie statement
            stmt.close();

            //7. Sprzątanie - zamykanie połączenia
            conn.close();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        finally {
            if(stmt != null){
                try{
                    stmt.close();
                }
                catch (SQLException e){
                    e.printStackTrace();
                }
            }

            if(conn != null){
                try{
                    conn.close();
                }
                catch (SQLException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
