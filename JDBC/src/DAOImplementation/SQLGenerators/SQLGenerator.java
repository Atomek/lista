package DAOImplementation.SQLGenerators;

import DAOImplementation.Entities.Entity;

public interface SQLGenerator<T extends Entity> {
    String insert(T toInsert);
    String selectAll();
    String update(T toUpdate);
    String delete(T toDelete);
}
