package DAOImplementation.DAOs;

import DAOImplementation.BaseDAO;
import DAOImplementation.Entities.Employee;
import DAOImplementation.SQLGenerators.EmployeeSQLGenerator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EmployeeDAO extends BaseDAO<Employee> {
    EmployeeSQLGenerator sqlGenerator = new EmployeeSQLGenerator();

    @Override
    public void insert(Employee toInsert) {
        sql = sqlGenerator.insert(toInsert);
        execute();
    }

    @Override
    public List<Employee> select() {
        sql = sqlGenerator.selectAll();
        return executeSelect();
    }

    @Override
    public void update(Employee toUpdate) {
        sql = sqlGenerator.update(toUpdate);
        execute();
    }

    @Override
    public void delete(Employee toDelete) {
        sql = sqlGenerator.delete(toDelete);
        execute();
    }

    @Override
    protected List<Employee> parse(ResultSet rs) {
        ArrayList<Employee> result = new ArrayList<>();

        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstName = rs.getString("firstname");
                String lastName = rs.getString("lastName");
                int age = rs.getInt("age");

                result.add(new Employee(
                        id, firstName, lastName, age
                ));
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return result;
    }
}
