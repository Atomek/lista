package DAOImplementation;

import DAOImplementation.DAOs.EmployeeDAO;
import DAOImplementation.Entities.Employee;

import java.util.List;

public class Demo {
    public static void main(String[] args) {
//        Employee employee =
//                new Employee(1111, "Dupa",
//                        "Dupa", 12);

        EmployeeDAO dao = new EmployeeDAO();
        List<Employee> emp = dao.select();
        for (Employee employee : emp) {
            System.out.println(employee);
        }

    }
}
