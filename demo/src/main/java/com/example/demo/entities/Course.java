package com.example.demo.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Course {
    //kodFirst najlpierw tworzone są klasy poźniej tabele
    @Id                 //klucz głowny
    @GeneratedValue     //Hibernate ma się zatroszczyć zeby klucz głowny ma byc niepowtarzalny
    private Long id;

    private String name;

    protected Course() {
    }

    public Course(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
