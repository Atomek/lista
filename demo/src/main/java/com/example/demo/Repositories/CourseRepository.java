package com.example.demo.Repositories;

import com.example.demo.entities.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class CourseRepository {
    @Autowired
    EntityManager em;

    public Course findById(long id){
        return em.find(Course.class, id); //(co ma wzracać, względem czego)
    }

}
