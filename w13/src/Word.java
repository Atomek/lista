import java.util.Scanner;

public class Word {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String wordIn = "";

        do {
            wordIn = scanner.nextLine();
        } while ((wordIn.length() - 1) % 2 != 0);

        //rozłożenie

        for (int i = 0; i < (wordIn.length() + 1) / 2; i++) {
            for (int j = 0; j < wordIn.length() - i; j++) {
                System.out.print(".");
            }
            int halfWord = ((wordIn.length() - 1) / 2);
            if (halfWord - i >= 0) {
                for (int point = halfWord - i; point <= halfWord + i; point++) {
                    System.out.print(wordIn.charAt(point));
                }
            }
            for (int j = 0; j < wordIn.length() - i; j++) {
                System.out.print(".");
            }
            System.out.println();
        }

    }
}

