import java.util.ArrayList;
import java.util.List;

public class Zadanie1 {
    public static void main(String[] args) {

        List<String> namesList = new ArrayList<String>();

        namesList.add("Matylda");
        namesList.add("Papryk");
        namesList.add("Pioter");
        namesList.add("Janusz");
        namesList.add("Grażyna");



        for(int index =0; index<namesList.size(); index++){
            String temp = namesList.get(index);
            System.out.println("Index[" + index + "]" + " Name: "  + temp);
        }

        System.out.println("Size of ArrayList: " + namesList.size());

        for (String name:namesList) {
            System.out.println(namesList.indexOf(name) + name);
        }
    }
}
