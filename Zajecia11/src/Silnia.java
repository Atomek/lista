import java.util.Scanner;

public class Silnia {
    public static void main(String[] args) {
        System.out.println(factorialRek(5));
        System.out.println(factorialRek(10));
        System.out.println(factorial(10));

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj Silnie: ");
        System.out.println(factorialRek(scanner.nextInt()));
    }

    public static int factorial(int y){
        int result=1;
        for (int counter = 1; counter <= y; counter++) {
            result=result*counter;
        }
        return result;
    }

    public static int factorialRek(int y){

        if(y==0){
            return 1;
        }
        else
        return factorialRek(y-1) *y;

    }
}
