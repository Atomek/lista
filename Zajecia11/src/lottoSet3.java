import java.util.*;

public class lottoSet3 {
    public static void main(String[] args) {
        Set<Integer> lottoSet = new HashSet<>();
        Scanner scanner = new Scanner(System.in);
        while(lottoSet.size()<6){

            Boolean isInRange = false;
            Integer number = 0;
            do {
                try{
                    number = scanner.nextInt();


                isInRange= number >=6 || number<=49;
                if(isInRange){
                    System.out.println("ok");
                }
                else{
                    System.out.println("Error");
                }}catch (InputMismatchException e){
                    System.out.println("error");
                    scanner.next();
                }
            }while(!isInRange);

            if(lottoSet.contains(number)){
                System.out.println("Number exist");
            }
            else{
                System.out.println("ok");
            }
            lottoSet.add(number);
        }
        System.out.println(lottoSet);
    }
}
