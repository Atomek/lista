import javafx.scene.control.RadioButton;

import java.util.Queue;
import java.util.Random;

public class ArrayDeque {
    public static void main(String[] args) {
        Random generator = new Random();

        Queue<Integer> queue = new java.util.ArrayDeque<>();

        for (int i = 0; i < 10; i++) {
            queue.add(generator.nextInt(100));
        }

        System.out.println(queue);

        while (!queue.isEmpty()) {
            int y = queue.poll();
            if (y % 2 == 0) {
                System.out.println(y);
            }
        }
    }
}
