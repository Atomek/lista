package drinkee;

public abstract class SkładnikiDecorator extends Napoj {
    @Override
    public abstract String pobierzOpis();
}
