package Window;

import javax.swing.*;

public class MainAppWindow extends JFrame {
    private String welcomeMassage;

    private static final MainAppWindow INSTANCE = new MainAppWindow();

    private JButton jButton;

    private MainAppWindow(){
        super("WINDOW");
        this.jButton = new JButton("button");
        this.jButton.addActionListener((event) ->{
            System.out.println("hello");
        });
        this.add(jButton);
        this.setSize(1000,1000);
    }
    static MainAppWindow getInstance(){
        return INSTANCE;
    }
    public static void showWindow(){
        INSTANCE.setVisible(true);
    }

}
