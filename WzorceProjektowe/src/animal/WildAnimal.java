package animal;

public class WildAnimal {

    private String spacesName;
    private int legsCount;
    private boolean isPredator;
    private double weight;
    private String diet;

    public WildAnimal(String spacesName, int legsCount, boolean isPredator, double weight, String diet){
        this.isPredator=isPredator;
        this.legsCount=legsCount;
        this.spacesName=spacesName;
        this.weight=weight;
        this.diet=diet;
    }

    public String getSpacesName() {
        return spacesName;
    }

    public int getLegsCount() {
        return legsCount;
    }

    public boolean isPredator() {
        return isPredator;
    }

    public void setSpacesName(String spacesName) {
        this.spacesName = spacesName;
    }

    public void setLegsCount(int legsCount) {
        this.legsCount = legsCount;
    }

    public void setPredator(boolean predator) {
        isPredator = predator;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getDiet() {
        return diet;
    }

    public void setDiet(String diet) {
        this.diet = diet;
    }

    @Override
    public String toString() {
        return "WildAnimal{" +
                "spacesName='" + spacesName + '\'' +
                ", legsCount=" + legsCount +
                ", isPredator=" + isPredator +
                ", weight=" + weight +
                ", diet='" + diet + '\'' +
                '}';
    }
}
