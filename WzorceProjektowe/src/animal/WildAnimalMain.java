package animal;

public class WildAnimalMain {
    public static void main(String[] args) {
        WildAnimal wildAnimal = new WildAnimalBuilder("oRanyCycoJebka")
                .setIsPredator(false)
                .setLegsCount(5)
                .createWildAnimal();

        System.out.println(wildAnimal.toString());


        WildAnimal wildAnimal1 = new WildAnimalBuilder("extra")
                .setWeight(23.4)
                .setIsPredator(true)
                .setDiet("Sun")
                .setLegsCount(8)
                .createWildAnimal();

        System.out.println(wildAnimal1.toString());
    }

}
