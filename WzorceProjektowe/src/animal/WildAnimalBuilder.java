package animal;

public class WildAnimalBuilder {

    private String spacesName;
    private int legsCount;
    private boolean isPredator;
    private double weight;
    private String diet;

    public WildAnimalBuilder(String spacesName) {
        this.spacesName = spacesName;
    }

    public WildAnimalBuilder setLegsCount(int legsCount) {
        this.legsCount = legsCount;
        return this;
    }
    public WildAnimalBuilder setIsPredator(boolean isPredator){
        this.isPredator=isPredator;
        return this;
    }
    public WildAnimalBuilder setWeight(double weight){
        this.weight=weight;
        return this;
    }
    public WildAnimalBuilder setDiet(String diet){
        this.diet=diet;
        return this;
    }


    public WildAnimal createWildAnimal() {
        return new WildAnimal(spacesName, legsCount, isPredator, weight, diet);
    }
}
